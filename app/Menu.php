<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Menu extends Model
{
    const CACHE_TTL = 5;

    protected $layers = [];

    protected $fillable = [
        "field",
        "max_depth",
        "max_children"
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->layers = Cache::get('layers_for_' . $this->id);
    }

    /**
     * Hidden properties that we don't want to expose in the API output
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'id'
    ];

    public function items() {
        return $this->hasMany('App\Item', 'menu_id');
    }

    /**
     * Composes the layers for the menu and stores that in cache. This caching solution is not the best, ideally it
     * should per layer or per node.
     */
    public function composeLayers() {
        Cache::forget('layers_for_' . $this->id);
        $topLevel = $this->items()->whereNull('parent_id')->get()->each(function($item) {
            $item->setDepth(0); // top layer is first layer
        });

        $this->addChildrenToLayer($topLevel);
        Cache::put('layers_for_' . $this->id, $this->layers, self::CACHE_TTL);
    }

    private function addChildrenToLayer($children) {
        if (!empty($children)) {
            foreach ($children as $child) {
                $this->layers[$child->getDepth()][] = $child;
                $this->addChildrenToLayer($child->getChildren());
            }
        }
    }

    /**
     * Retrieves the items that are at a specific depth in the hierarchy (i.e the layer)
     * @param int $layer
     * @return mixed|null
     */
    public function getLayer($layer = 0) {
        if (empty($this->layers)) {
            $this->composeLayers();
        }
        return !empty($this->layers[$layer]) ? $this->layers[$layer] : [];
    }

    public function getLayers() {
        if (empty($this->layers)) {
            $this->composeLayers();
        }
        return $this->layers;
    }

    /**
     * @param array $layers
     * @param Item $item
     * @return mixed
     */
    private function insertItemIntoLayer($layers, $item) {
        $layers[$item->getDepth()] = $item;
        return $layers;
    }
}
