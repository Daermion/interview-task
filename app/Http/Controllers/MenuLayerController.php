<?php

namespace App\Http\Controllers;

use App\Item;
use App\Menu;
use Illuminate\Http\Request;

class MenuLayerController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param mixed $menu
     * @param int $layer The layer depth that we want
     * @return void
     */
    public function show(Menu $menu, $layer)
    {
        // TO-DO: add layer cache
        $layer = $menu->getLayer($layer);

        return response()->json($layer, '200');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $menu
     * @param int $layer The layer depth that we want
     * @return void
     * @throws \Exception
     */
    public function destroy(Menu $menu, $layer)
    {
        $targetLayer = $menu->getLayer($layer);

        if (!empty($targetLayer)) {
            $children = $menu->getLayer(($layer+1));
            if (!empty($children)) {
                // relink the children first
                /**
                 * @var Item $child
                 */
                foreach ($children as $child) {
                    $parent = $child->getParent();
                    $child->parent_id = $parent->parent_id;
                    $child->save();
                }
            }
            // now delete everything in the layer
            /**
             * @var Item $item
             */
            foreach ($targetLayer as $item) {
                $item->delete();
            }
            // recalculate and invalidate cache
            $menu->composeLayers();

            return response()->json("Layer deleted.", 200);
        }

        return response()->json("No layer to delete", 404);
    }
}
