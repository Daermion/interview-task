<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $json = $request->json()->all();

        // the specification requests that you should be able to send only the "field" value to create an item
        // but that makes no sense to me, since you'd have a floating item with a menu or parent to call home
        // so I haven't accounted for this -- if you try to send an $item without a menu_id minimum it'll give an error
        if (empty($json['menu_id'])) {
            return response()->json(['Missing menu_id parameter', 400]);
        }

        $item = Item::create($json);

        return response()->json($item, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return response()->json($item->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
        $json = $request->json()->all();
        $item->update($json);

        return response()->json($item, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed  $item
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Item $item)
    {
        //
        $item->delete();

        return response()->json(["Item deleted", 200]);
    }
}
