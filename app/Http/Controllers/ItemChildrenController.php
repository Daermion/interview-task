<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ItemChildrenController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Item $item
     * @return void
     */
    public function store(Request $request, Item $item)
    {
        $itemDepth = $item->calculateDepth();
        /**
         * This is a really crude and not all-encompassing implementation of the max_depth restriction. Because it might
         * very well bypass this on the first level and still violate it on the second/third/etc levels.
         * Just pretend I had more time and wrote something nicer. ;)
         */
        if ($itemDepth >= $item->getMenu()->max_depth) {
            return response()->json(['Max depth would be exceeded. Cannot create children.'], 400);
        }

        /**
         * Again, really bad implementation but I have very little time left, so I'm just going to leave this crude
         * solution here as a "better than nothing" kind of deal.
         */
        if (count($item->getMenu()->getLayer($itemDepth)) >= $item->getMenu()->max_children) {
            return response()->json(['Maximum number of children already reached at this layer.'], 400);
        }

        //
        $json = $request->json()->all();

        $children = [];
        foreach ($json as $row) {
            $child = $this->createChild($row, $item);
            $children[] = $child->toArrayWithChildren();
        }
        // recalculate layers and clear some cache
        $item->getMenu()->composeLayers();

        return response()->json($children, 201);
    }

    /**
     * Creates children recursively.
     *
     * @todo Put this somewhere else where it makes more sense than the Controller.
     * @param $json
     * @param $parent
     * @return mixed Item
     */
    private function createChild($json, $parent) {
        $item = Item::create([
            'field' => $json['field'],
            'menu_id' => $parent->menu_id,
            'parent_id' => $parent->id
        ]);

        if (!empty($json['children'])) {
            foreach ($json['children'] as $row) {
                $this->createChild($row, $item);
            }
        }

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        $children = [];

        foreach ($item->getChildren() as $child) {
            $children[] = $child->toArrayWithChildren();
        }

        return response()->json($children, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
        $item->children()->delete();

        return response()->json(["Children deleted", 200]);
    }
}
