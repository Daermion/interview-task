<?php

namespace App\Http\Controllers;

use App\Menu;

class MenuDepthController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        $data = [
            'depth' => count($menu->getLayers())
        ];

        return response()->json($data, 200);
    }
}
