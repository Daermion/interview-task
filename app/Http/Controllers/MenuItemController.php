<?php

namespace App\Http\Controllers;

use App\Item;
use App\Menu;
use Illuminate\Http\Request;

class MenuItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        // only retrieve top level items because the structure is built recursively
        $items = $menu->items()->whereNull('parent_id')->get();

        $children = [];

        /**
         * @var $child Item
         */
        foreach ($items as $child) {
            $children[] = $child->toArrayWithChildren();
        }

        return response()->json($children, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->items()->delete();

        return response()->json(["Menu items deleted."], 200);
    }
}
