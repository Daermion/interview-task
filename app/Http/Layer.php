<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;

class Layer
{
    /**
     * Id of the menu object to which this layer belongs
     *
     * @var int
     */
    protected $menuId = null;
    /**
     * The immediate parent of this layer, if 'null' it means it is the top layer of the menu
     *
     * @var int
     */
    protected $parentId = null;
    /**
     * Collection of menu items in the layer
     *
     * @var Collection
     */
    protected $items = null;

    /**
     * The depth of the menu hierarchy at which this layer resides
     *
     * @var int
     */
    protected $depth = 0;

    /**
     * Layer constructor.
     * @param $parent Item
     * @param int $depth
     */
    public function __construct($parent, $depth = 0)
    {
        $this->items = $parent->children()->get();
        $this->depth = $parent->getDepth();
    }

    public function getDepth() {
        return $this->depth;
    }

    public function getItems() {
        return $this->items;
    }
}
