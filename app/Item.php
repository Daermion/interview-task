<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $depth = null;

    protected $fillable = [
        'field',
        'menu_id',
        'parent_id'
    ];

    /**
     * Hidden properties that we don't want to expose in the API output
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'menu_id', 'parent_id', 'id'
    ];

    public function getDepth() {
        return (!is_null($this->depth)) ? $this->depth : $this->calculateDepth();
    }

    public function setDepth($depth = 0) {
        $this->depth = $depth;
    }

    public function calculateDepth() {
        return $this->depth = count($this->getParents()) + 1;
    }

    public function children() {
        return $this->hasMany('App\Item', 'parent_id');
    }

    /**
     * Recursively applies set depth to all children, it's less taxing doing it this way than calculating it per layer.
     * @return Collection
     */
    public function getChildren() {
        return $this->children()->get()->each(function ($item, $key) {
            $item->setDepth($this->getDepth()+1);
        });
    }

    public function getParent() {
        return $this->belongsTo('App\Item', 'parent_id')->first();
    }

    public function getMenu() {
        return $this->belongsTo('App\Menu', 'menu_id')->first();
    }

    /**
     * Retrieve the hierarchy of all parents, this is mostly to we can figure out how deep we are in the structure.
     * @return Collection
     */
    protected function getParents() {
        $parents = new Collection();

        $parent = $this->getParent();
        while(is_a($parent, self::class)) {
            $parents->push($parent);
            $parent = $parent->getParent();
        }

        return $parents;
    }

    /**
     * Recursively putting together the tree structure for the JSON output
     *
     * @return array
     */
    public function toArrayWithChildren() {
        $children = [];

        foreach ($this->getChildren() as $child) {
            $children[] = $child->toArrayWithChildren();
        }

        return array_merge($this->toArray(), [
            "children" => $children
        ]);
    }
}
